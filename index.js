// Create
db.users.insertOne({
    "firstName": "Nigel",
    "lastName": "Uno",
    "email": "numbuh1@mail.com",
    "password": "number1",
    "isAdmin": false
})

db.users.insertMany([
    {
        "firstName": "Hogarth",
        "lastName": "Pennywhistle",
        "email": "numbuh2@mail.com",
        "password": "number2",
        "isAdmin": false
    },
    {
        "firstName": "Kuki",
        "lastName": "Sanban",
        "email": "numbuh3@mail.com",
        "password": "number3",
        "isAdmin": false
    },
    {
        "firstName": "Wallabee",
        "lastName": "Beetles",
        "email": "numbuh4@mail.com",
        "password": "number4",
        "isAdmin": false
    },
    {
        "firstName": "Abigail",
        "lastName": "Lincoln",
        "email": "numbuh5@mail.com",
        "password": "number5",
        "isAdmin": false
    }
])
    

db.courses.insertOne({
    "name": "HTML 101",
    "price": 1000,
    "isActive": false
})

db.courses.insertMany([
    {
        "name": "CSS 101",
        "price": 1500,
        "isActive": false
    },
    {
        "name": "JavaScript 101",
        "price": 2000,
        "isActive": false
    }
])


// Read
db.users.find({"isAdmin": false})


// Update
db.users.updateOne({},{$set:{"isAdmin": true}})

db.courses.updateOne({},{$set:{"isActive": true}})


// Delete
db.courses.deleteMany({"isActive": false})